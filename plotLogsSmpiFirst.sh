#!/bin/bash

rank=${1:-0}

deno run -A expected_model_change_log.ts \
    ./logs/Optimized/logSMPI_5000/$rank.csv \
    ./logs/Optimized/logS4BXI_5000/$rank.csv > ./logs/Optimized/SMPI_first.csv

Rscript plotLogs.R \
    ./logs/Optimized/logS4BXI_5000/$rank.csv \
    ./logs/Optimized/logSMPI_5000/$rank.csv \
    ./logs/Optimized/logModelChangeMostlySMPI_5000/$rank.csv \
    ./logs/Optimized/SMPI_first.csv \
    ./logs/Optimized/plot_SMPI_first.png
