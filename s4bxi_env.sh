#!/bin/bash

export S4BXI_INSTALL_ROOT=${S4BXI_INSTALL_ROOT:-"/opt"}
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${S4BXI_INSTALL_ROOT}/s4bxi/lib:${S4BXI_INSTALL_ROOT}/ompi_bull/lib:${S4BXI_INSTALL_ROOT}/simgrid/lib64:${S4BXI_INSTALL_ROOT}/simgrid/lib:./"
export S4BXI_KEEP_TEMPS=0
export S4BXI_NO_DLCLOSE=0
export S4BXI_PRIVATIZE_LIBS="libmpi.so.40;libopen-rte.so.40;libopen-pal.so.40"
export S4BXI_CPU_FACTOR=0.25
export S4BXI_CPU_THRESHOLD=1e-8
export S4BXI_CPU_ACCUMULATE=0
export S4BXI_E2E_OFF=0
export S4BXI_QUICK_ACKS=0
export S4BXI_MODEL_PCI_COMMANDS=1
export OMPI_MCA_btl=bxi,self
export OMPI_MCA_pml=ob1
export OMPI_MCA_osc=
# export OMPI_MCA_pml=bxi
# export OMPI_MCA_pml_bxi_enable_pml_bxi=1
export OMPI_MCA_mtl=
export OMPI_MCA_coll="tuned,basic,libnbc,self"
export OMPI_MCA_btl_bxi_eq_recv_size=10001
export OMPI_MCA_btl_bxi_recv_me_size=5000000
export OMPI_MCA_btl_bxi_recv_me_num=64
export OMPI_MCA_btl_bxi_min_recv_me_active=16
export OMPI_MCA_btl_vader_single_copy_mechanism=emulated

 # Should be default, but let's specify it since it's very important and the default value might change in the future
export OMPI_MCA_mpi_common_bxi_device_detection=false

# Also default, but it's important to be sure this is a fixed and known value
# export OMPI_MCA_btl_bxi_rndv_eager_limit=8192

# export OMPI_MCA_btl_base_verbose=200

# --log=s4bxi.thres:debug \

export SIMGRID_PATH="${SIMGRID_PATH:=$S4BXI_INSTALL_ROOT/simgrid}"
export WORKSPACE="${WORKSPACE:=$PWD}"
export PLATFORMDIR=$WORKSPACE/s4bxi-platforms
export CORES=$(grep ^cpu\\scores /proc/cpuinfo | uniq | awk '{print $4}')
# https://github.com/open-mpi/ompi/issues/10142#issuecomment-1071929869
export ZES_ENABLE_SYSMAN=1
