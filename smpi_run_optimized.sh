#!/bin/bash

source ./s4bxi_env.sh

out_prefix=tmp
input_file=src/HPL/HPL.dat.dist
patch=src/HPL/patch_hpl_optimization.diff
makefile=src/HPL/Make.SMPI
sizes="20000"
ppn=4
node_number=4

while getopts ":i:o:p:m:s:c:N:h" opt; do
  case $opt in
    h) echo "Usage: ./smpi_run_optimized.sh -i input_file -p patch -o out_prefix -m makefile -s input_size_array -w wrapper -c ppn -N nodes"
    echo "All the options have default values"
    exit 0
    ;;
    i) input_file="$OPTARG"
    ;;
    p) patch="$OPTARG"
    ;;
    o) out_prefix="$OPTARG"
    ;;
    m) makefile="$OPTARG"
    ;;
    s) sizes="$OPTARG"
    ;;
    c) ppn="$OPTARG"
    ;;
    N) node_number="$OPTARG"
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    exit 1
    ;;
  esac
done

base_folder="$WORKSPACE/results/Optimized"
mkdir -p $base_folder
out_files="$base_folder/$out_prefix"

echo "Clean up the place"

rm -rf $WORKSPACE/Benchmarks/HPL/Optimized
mkdir -p $WORKSPACE/Benchmarks/HPL/Optimized
cd $WORKSPACE/Benchmarks/HPL || exit 1
echo "Download the source code"
test -e hpl-2.2.tar.gz || curl -o hpl-2.2.tar.gz -Lkf http://www.netlib.org/benchmark/hpl/hpl-2.2.tar.gz

echo "Unpack the code"
tar -xf hpl-2.2.tar.gz -C Optimized
mv Optimized/hpl-2.2/* Optimized
rmdir Optimized/hpl-2.2

echo "Install the modified sources"
cd Optimized || exit 1
cp $WORKSPACE/$makefile ./Make.SMPI
mkdir -p bin/SMPI
cp $WORKSPACE/src/HPL/*.py bin/SMPI
cp $WORKSPACE/src/HPL/dahu.xml bin/SMPI/
cp $WORKSPACE/src/HPL/dgemm_model.c src/blas/
rm bin/SMPI/hosts.txt || true
touch bin/SMPI/hosts.txt
patch -p1 < $WORKSPACE/$patch
for i in $( seq 1 $node_number ); do
    for j in $( seq 1 $ppn ); do
        # echo "pm0-nod${i}" >> bin/SMPI/hosts.txt
        echo "amour${i}_CPU$(( $j - 1 ))" >> bin/SMPI/hosts.txt
    done
done

echo "Compile it"
sed -ri "s|TOPdir\s*=.+|TOPdir="`pwd`"|g" Make.SMPI
make -s startup arch=SMPI
make -s SMPI_OPTS="-DSMPI_OPTIMIZATION -DSMPI_SEED=105" arch=SMPI

cd bin/SMPI || exit 1
for in_size in $sizes; do
    echo "Generate input file"
    sed -e "s/#%PROBLEM_SIZE%#/${in_size}/g" \
        -e "s/#%PGRID_P%#/${node_number}/g" \
        -e "s/#%PGRID_Q%#/${ppn}/g" \
        $WORKSPACE/$input_file > ./HPL.dat
#--cfg=smpi/shared-malloc-blocksize:2097152
    echo "Run app with out file ${out_files}_${in_size}.*"
    (time smpirun --cfg=smpi/privatization:dlopen \
                  --cfg=smpi/display-timing:yes \
                  --cfg=smpi/shared-malloc-blocksize:2097152 \
                  --cfg=smpi/host-speed:$(bc <<< "10000 * ${S4BXI_CPU_FACTOR}")Mf \
                  -hostfile ./hosts.txt \
                  -platform $PLATFORMDIR/amour/build/libamour.so \
                  -np $(( $node_number * $ppn )) xhpl
    ) 1> "${out_files}_${in_size}.txt" 2> "${out_files}_${in_size}.log"
done
