import { basename } from "https://deno.land/std@0.136.0/path/mod.ts";

const HEADER_LINE =
  "T/V                N    NB     P     Q               Time                 Gflops";
const HEADER_SEP =
  "--------------------------------------------------------------------------------";

interface InterestingData {
  N: number;
  nodes: number;
  ppn: number;
  Gflops: number;
  wallClockTime: number;
}

const parseFile = (filename: string): InterestingData[] => {
  const data = Deno.readTextFileSync(filename).split(/\r?\n/);
  const metadata = Deno.readTextFileSync(
    `${filename.substring(0, filename.lastIndexOf("."))}.log`,
  ).split(/\r?\n/);

  let wallClockTime = -1;
  for (const m of metadata) {
    const wallClockExpr = m.match(/real\s+([0-9]+)m([0-9\.]+)s/);
    if (wallClockExpr) {
      wallClockTime = 60 * Number.parseInt(wallClockExpr[1]) +
        Number.parseFloat(wallClockExpr[2]);
    }
  }

  let lineIsHeader = false;
  let nextLineIsData = false;
  const out: InterestingData[] = [];
  for (let i = 0; i < data.length; i++) {
    if (nextLineIsData) {
      const [tv, n, nb, p, q, time, gflops] = data[i].split(" ")
        .filter((s) => s.length);
      out.push({
        N: +n,
        nodes: +p,
        ppn: +q,
        Gflops: Number.parseFloat(gflops),
        wallClockTime: wallClockTime,
      });
      nextLineIsData = false;
      lineIsHeader = false;
      continue;
    }

    // Next line should be separator
    if (data[i] === HEADER_LINE) {
      lineIsHeader = true;
      continue;
    }

    // Next line has useful data
    if (lineIsHeader && data[i] === HEADER_SEP) {
      nextLineIsData = true;
      lineIsHeader = false;
      continue;
    }
  }

  return out;
};

console.log("Model, N, nodes, ppn, Gflops, WallClockTime");
Deno.args.map((f) => {
  parseFile(f).map((o: InterestingData) =>
    console.log(
      [
        basename(f).split(/[_\.]/)[0],
        o.N,
        o.nodes,
        o.ppn,
        o.Gflops,
        o.wallClockTime,
      ].join(", "),
    )
  );
});
