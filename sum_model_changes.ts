import { expandGlobSync } from "https://deno.land/std@0.136.0/fs/mod.ts";
import { readCSVObjects } from "https://deno.land/x/csv@v0.7.4/mod.ts";

const folder = Deno.args[0];

for (const file of expandGlobSync(`${folder}/*.csv`)) {
  const sums: Record<string, number> = { Total: 0 };
  for await (
    const obj of readCSVObjects(await Deno.open(file.path), {
      columnSeparator: ", ",
    })
  ) {
    const delta = Number.parseFloat(obj.delta);
    if (!sums[obj.comment]) {
      sums[obj.comment] = delta;
      sums.Total += delta;
    } else {
      sums[obj.comment] += delta;
      sums.Total += delta;
    }
  }
  console.log(
    ` === Rank ${file.name.substring(0, file.name.lastIndexOf("."))} === `,
  );
  console.table(sums);
}
