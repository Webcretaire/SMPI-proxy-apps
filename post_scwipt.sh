#!/bin/bash

size=10000
deno run -A diffManyLogs.ts logs/ManyLogs_*/modelChange_$size/*.csv > logs/ManyLogsModelChange$size.csv
deno run -A diffManyLogs.ts logs/ManyLogs_*/s4bxi_$size/*.csv > logs/ManyLogsS4BXI$size.csv
deno run -A diffManyLogs.ts logs/ManyLogs_*/smpi_$size/*.csv > logs/ManyLogsSMPI$size.csv
