#!/bin/bash

ppn=${1:-4}
nodes=${2:-4}
sizes=${3:-10000}
out_prefix=${4}

echo "Run tests of size ${sizes} with ${ppn} ppn on ${nodes} nodes"

./smpi_run_optimized.sh \
    -p src/HPL/patch_hpl_optimization.diff \
    -s "$sizes" \
    -o "${out_prefix}smpirun_${nodes}nodes_${ppn}ppn" \
    -m src/HPL/Make.SMPI.amour \
    -i src/HPL/HPL.dat.144.dist \
    -c ${ppn} -N ${nodes}
S4BXI_SMPI_IMPLEM=0 ./s4bxi_run_optimized.sh \
    -p src/HPL/patch_hpl_optimization_S4BXI_model_change.diff \
    -s "$sizes" -o "${out_prefix}model_change_${nodes}nodes_${ppn}ppn" \
    -m src/HPL/Make.S4BXI.amour \
    -i src/HPL/HPL.dat.144.dist \
    -c ${ppn} -N ${nodes}
S4BXI_SMPI_IMPLEM=1 ./s4bxi_run_optimized.sh \
    -p src/HPL/patch_hpl_optimization_S4BXI_neutral.diff \
    -s "$sizes" \
    -o "${out_prefix}smpi_${nodes}nodes_${ppn}ppn" \
    -m src/HPL/Make.S4BXI.amour \
    -i src/HPL/HPL.dat.144.dist \
    -c ${ppn} -N ${nodes}
S4BXI_SMPI_IMPLEM=0 ./s4bxi_run_optimized.sh \
    -p src/HPL/patch_hpl_optimization_S4BXI_neutral.diff \
    -s "$sizes" \
    -o "${out_prefix}s4bxi_${nodes}nodes_${ppn}ppn" \
    -m src/HPL/Make.S4BXI.amour \
    -i src/HPL/HPL.dat.144.dist \
    -c ${ppn} -N ${nodes}
