import { readCSVObjects } from "https://deno.land/x/csv@v0.7.4/mod.ts";

const log1 = await Deno.open(Deno.args[0]);
const log2 = await Deno.open(Deno.args[1]);

async function toArray<T>(asyncIterator: AsyncIterable<T>): Promise<T[]> {
  const arr = [];
  for await (const i of asyncIterator) arr.push(i);

  return arr;
}

const csv1 = await toArray(readCSVObjects(log1, { columnSeparator: ", " }));
const csv2 = await toArray(readCSVObjects(log2, { columnSeparator: ", " }));

let totalTime = 0;

console.log("magic, rank, use_smpi, time, delta, comment");
for (let i = 0; i < csv1.length; ++i) {
  let chosenCsv: any;

  if (i == 0) chosenCsv = csv1[i];
  else chosenCsv = (i % 2 == 1 ? csv1 : csv2)[i];

  const delta = Number.parseFloat(chosenCsv.delta);
  totalTime += delta;

  console.log(
    Object.values({
      ...chosenCsv,
      time: totalTime.toFixed(6),
      delta: delta,
    }).join(", "),
  );
}
