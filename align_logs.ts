import { readCSVObjects } from "https://deno.land/x/csv@v0.7.4/mod.ts";

let timeToRemove = -1;
const file = await Deno.open(Deno.args[0]);
console.log("magic, rank, use_smpi, time, delta, comment");

for await (const obj of readCSVObjects(file, { columnSeparator: ", " })) {
  if (obj.comment == "Initial setup") {
    continue;
  }

  if (timeToRemove === -1) {
    const delta = Number.parseFloat(obj.delta);
    timeToRemove = delta;
  }

  console.log(
    Object.values({
      ...obj,
      time: (Number.parseFloat(obj.time) - timeToRemove).toFixed(6)
    }).join(", "),
  );
}
