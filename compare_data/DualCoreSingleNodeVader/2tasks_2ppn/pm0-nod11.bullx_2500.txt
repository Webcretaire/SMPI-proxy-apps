================================================================================
HPLinpack 2.2  --  High-Performance Linpack benchmark  --   February 24, 2016
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :    2500 
NB     :     120 
PMAP   : Row-major process mapping
P      :       1 
Q      :       2 
PFACT  :    Left    Crout    Right 
NBMIN  :       2        4 
NDIV   :       2 
RFACT  :    Left    Crout    Right 
BCAST  :   1ring 
DEPTH  :       0 
SWAP   : Mix (threshold = 64)
L1     : transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00L2L2        2500   120     1     2               0.24              4.346e+01
HPL_pdgesv() start time Wed Jun  1 19:41:04 2022

HPL_pdgesv() end time   Wed Jun  1 19:41:04 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0066034 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00L2L4        2500   120     1     2               0.24              4.383e+01
HPL_pdgesv() start time Wed Jun  1 19:41:04 2022

HPL_pdgesv() end time   Wed Jun  1 19:41:04 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0067036 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00L2C2        2500   120     1     2               0.24              4.388e+01
HPL_pdgesv() start time Wed Jun  1 19:41:04 2022

HPL_pdgesv() end time   Wed Jun  1 19:41:05 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0066034 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00L2C4        2500   120     1     2               0.24              4.414e+01
HPL_pdgesv() start time Wed Jun  1 19:41:05 2022

HPL_pdgesv() end time   Wed Jun  1 19:41:05 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0059701 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00L2R2        2500   120     1     2               0.24              4.401e+01
HPL_pdgesv() start time Wed Jun  1 19:41:05 2022

HPL_pdgesv() end time   Wed Jun  1 19:41:05 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0066034 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00L2R4        2500   120     1     2               0.24              4.415e+01
HPL_pdgesv() start time Wed Jun  1 19:41:05 2022

HPL_pdgesv() end time   Wed Jun  1 19:41:06 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0047715 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00C2L2        2500   120     1     2               0.24              4.378e+01
HPL_pdgesv() start time Wed Jun  1 19:41:06 2022

HPL_pdgesv() end time   Wed Jun  1 19:41:06 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0066034 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00C2L4        2500   120     1     2               0.24              4.401e+01
HPL_pdgesv() start time Wed Jun  1 19:41:06 2022

HPL_pdgesv() end time   Wed Jun  1 19:41:07 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0067036 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00C2C2        2500   120     1     2               0.24              4.382e+01
HPL_pdgesv() start time Wed Jun  1 19:41:07 2022

HPL_pdgesv() end time   Wed Jun  1 19:41:07 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0066034 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00C2C4        2500   120     1     2               0.24              4.415e+01
HPL_pdgesv() start time Wed Jun  1 19:41:07 2022

HPL_pdgesv() end time   Wed Jun  1 19:41:07 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0059701 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00C2R2        2500   120     1     2               0.24              4.388e+01
HPL_pdgesv() start time Wed Jun  1 19:41:07 2022

HPL_pdgesv() end time   Wed Jun  1 19:41:08 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0066034 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00C2R4        2500   120     1     2               0.24              4.407e+01
HPL_pdgesv() start time Wed Jun  1 19:41:08 2022

HPL_pdgesv() end time   Wed Jun  1 19:41:08 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0047715 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00R2L2        2500   120     1     2               0.24              4.385e+01
HPL_pdgesv() start time Wed Jun  1 19:41:08 2022

HPL_pdgesv() end time   Wed Jun  1 19:41:08 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0066034 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00R2L4        2500   120     1     2               0.24              4.408e+01
HPL_pdgesv() start time Wed Jun  1 19:41:09 2022

HPL_pdgesv() end time   Wed Jun  1 19:41:09 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0067036 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00R2C2        2500   120     1     2               0.24              4.397e+01
HPL_pdgesv() start time Wed Jun  1 19:41:09 2022

HPL_pdgesv() end time   Wed Jun  1 19:41:09 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0066034 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00R2C4        2500   120     1     2               0.24              4.415e+01
HPL_pdgesv() start time Wed Jun  1 19:41:09 2022

HPL_pdgesv() end time   Wed Jun  1 19:41:10 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0059701 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00R2R2        2500   120     1     2               0.24              4.400e+01
HPL_pdgesv() start time Wed Jun  1 19:41:10 2022

HPL_pdgesv() end time   Wed Jun  1 19:41:10 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0066034 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00R2R4        2500   120     1     2               0.24              4.414e+01
HPL_pdgesv() start time Wed Jun  1 19:41:10 2022

HPL_pdgesv() end time   Wed Jun  1 19:41:10 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0047715 ...... PASSED
================================================================================

Finished     18 tests with the following results:
             18 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
