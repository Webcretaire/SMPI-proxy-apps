================================================================================
HPLinpack 2.2  --  High-Performance Linpack benchmark  --   February 24, 2016
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :    1875 
NB     :     120 
PMAP   : Row-major process mapping
P      :       1 
Q      :       2 
PFACT  :    Left    Crout    Right 
NBMIN  :       2        4 
NDIV   :       2 
RFACT  :    Left    Crout    Right 
BCAST  :   1ring 
DEPTH  :       0 
SWAP   : Mix (threshold = 64)
L1     : transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00L2L2        1875   120     1     2               0.11              4.008e+01
HPL_pdgesv() start time Thu Jun  2 11:27:43 2022

HPL_pdgesv() end time   Thu Jun  2 11:27:43 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0057227 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00L2L4        1875   120     1     2               0.11              4.086e+01
HPL_pdgesv() start time Thu Jun  2 11:27:43 2022

HPL_pdgesv() end time   Thu Jun  2 11:27:43 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0049244 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00L2C2        1875   120     1     2               0.11              4.094e+01
HPL_pdgesv() start time Thu Jun  2 11:27:43 2022

HPL_pdgesv() end time   Thu Jun  2 11:27:43 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0057227 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00L2C4        1875   120     1     2               0.11              4.125e+01
HPL_pdgesv() start time Thu Jun  2 11:27:43 2022

HPL_pdgesv() end time   Thu Jun  2 11:27:43 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0051521 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00L2R2        1875   120     1     2               0.11              4.089e+01
HPL_pdgesv() start time Thu Jun  2 11:27:43 2022

HPL_pdgesv() end time   Thu Jun  2 11:27:43 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0057227 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00L2R4        1875   120     1     2               0.11              4.117e+01
HPL_pdgesv() start time Thu Jun  2 11:27:43 2022

HPL_pdgesv() end time   Thu Jun  2 11:27:44 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0058133 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00C2L2        1875   120     1     2               0.11              4.070e+01
HPL_pdgesv() start time Thu Jun  2 11:27:44 2022

HPL_pdgesv() end time   Thu Jun  2 11:27:44 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0057227 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00C2L4        1875   120     1     2               0.11              4.106e+01
HPL_pdgesv() start time Thu Jun  2 11:27:44 2022

HPL_pdgesv() end time   Thu Jun  2 11:27:44 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0049244 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00C2C2        1875   120     1     2               0.11              4.079e+01
HPL_pdgesv() start time Thu Jun  2 11:27:44 2022

HPL_pdgesv() end time   Thu Jun  2 11:27:44 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0057227 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00C2C4        1875   120     1     2               0.11              4.117e+01
HPL_pdgesv() start time Thu Jun  2 11:27:44 2022

HPL_pdgesv() end time   Thu Jun  2 11:27:44 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0051521 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00C2R2        1875   120     1     2               0.11              4.079e+01
HPL_pdgesv() start time Thu Jun  2 11:27:44 2022

HPL_pdgesv() end time   Thu Jun  2 11:27:45 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0057227 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00C2R4        1875   120     1     2               0.11              4.111e+01
HPL_pdgesv() start time Thu Jun  2 11:27:45 2022

HPL_pdgesv() end time   Thu Jun  2 11:27:45 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0058133 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00R2L2        1875   120     1     2               0.11              4.087e+01
HPL_pdgesv() start time Thu Jun  2 11:27:45 2022

HPL_pdgesv() end time   Thu Jun  2 11:27:45 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0057227 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00R2L4        1875   120     1     2               0.11              4.114e+01
HPL_pdgesv() start time Thu Jun  2 11:27:45 2022

HPL_pdgesv() end time   Thu Jun  2 11:27:45 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0049244 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00R2C2        1875   120     1     2               0.11              4.094e+01
HPL_pdgesv() start time Thu Jun  2 11:27:45 2022

HPL_pdgesv() end time   Thu Jun  2 11:27:45 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0057227 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00R2C4        1875   120     1     2               0.11              4.125e+01
HPL_pdgesv() start time Thu Jun  2 11:27:45 2022

HPL_pdgesv() end time   Thu Jun  2 11:27:46 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0051521 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00R2R2        1875   120     1     2               0.11              4.094e+01
HPL_pdgesv() start time Thu Jun  2 11:27:46 2022

HPL_pdgesv() end time   Thu Jun  2 11:27:46 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0057227 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00R2R4        1875   120     1     2               0.11              4.122e+01
HPL_pdgesv() start time Thu Jun  2 11:27:46 2022

HPL_pdgesv() end time   Thu Jun  2 11:27:46 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0058133 ...... PASSED
================================================================================

Finished     18 tests with the following results:
             18 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
