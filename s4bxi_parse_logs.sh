#!/bin/bash

source ./s4bxi_env.sh

out_folder=${1:-Vanilla}
slug=${2:-tmp}
num_procs=${3:-16}

input_file="$WORKSPACE/results/$out_folder/$slug.log"
out_path="$WORKSPACE/logs/$out_folder/$slug"
mkdir -p $out_path

for i in $(seq 0 $(( $num_procs - 1))); do
    echo "magic, rank, use_smpi, time, delta, comment" > $out_path/$i.csv
    cat $input_file | grep "#~#, $i," >> $out_path/$i.csv
done