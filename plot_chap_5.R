for (file in Sys.glob("./Rutil/*.R")) {
    source(file)
}

args <- commandArgs(trailingOnly = TRUE)

data <- read.csv(args[1])
s4bxi <- data[data$Model == "s4bxi",]
smpi <- data[data$Model == "smpirun",]

compare_data <- read.csv(args[2])

names(s4bxi) <- c("Model_ompi_s4bxi", "x", "nodes_ompi_s4bxi", "ppn_ompi_s4bxi", "Gflops_ompi_s4bxi", "WallClock_ompi_s4bxi")
names(smpi) <- c("Model_smpi", "x", "nodes_smpi", "ppn_smpi", "Gflops_smpi", "WallClock_smpi")
names(compare_data) <- c("Model_real", "x", "nodes_real", "ppn_real", "Gflops_real", "WallClock_real")

plotGflops <- melt_merge_3(s4bxi, smpi, compare_data, "Gflops_ompi_s4bxi", "Gflops_smpi", "Gflops_real") %>%
    summarySE() %>%
    plotLinePoint("Gflops", xLabel = "Matrix size (N)", xLog = TRUE, yLog = FALSE, xNBreaks = 10, yNBreaks = 5) + 
    expand_limits(y = 0)

plotWCT <- melt_merge_3(s4bxi, smpi, compare_data, "WallClock_ompi_s4bxi", "WallClock_smpi", "WallClock_real") %>%
    summarySE() %>%
    plotLinePoint("WallClock time (s)", xLabel = "Matrix size (N)", xLog = TRUE, yLog = TRUE, xNBreaks = 10) + 
    expand_limits(y = 1) + expand_limits(y = 10000)

png(args[3], width = 1500, height = 600)

grid.arrange(plotGflops, plotWCT, ncol = 2)

dev.off()