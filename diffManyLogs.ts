import { readCSVObjects } from "https://deno.land/x/csv@v0.7.4/mod.ts";

const files = [];
for (const f of Deno.args) {
  files.push(await Deno.open(f));
}

const data: Array<Array<Record<string, string>>> = files.map(() => []);

const comments: String[] = [];

let i = -1;
for (const file of files) {
  ++i;
  for await (const obj of readCSVObjects(file, { columnSeparator: ", " })) {
    data[i].push(obj);
    if (!comments.find((s) => s === obj.comment)) {
      comments.push(obj.comment);
    }
  }
}
console.log("magic, rank, use_smpi, time, delta, comment");

comments.forEach((comment) => {
  const times = data.flat()
    .filter((o: any) => o?.comment === comment)
    .map((o: any) => Number.parseFloat(o.time));
  const deltas = data.flat()
    .filter((o: any) => o?.comment === comment)
    .map((o: any) => Number.parseFloat(o.delta));
  console.log(
    Object.values({
      magic: "min",
      rank: data[0]?.[0]?.rank,
      use_smpi: data[0]?.[0]?.use_smpi,
      time: Math.min(...times),
      delta: Math.min(...deltas),
      comment: comment,
    }).join(", "),
  );
  console.log(
    Object.values({
      magic: "max",
      rank: data[0]?.[0]?.rank,
      use_smpi: data[0]?.[0]?.use_smpi,
      time: Math.max(...times),
      delta: Math.max(...deltas),
      comment: comment,
    }).join(", "),
  );
});
