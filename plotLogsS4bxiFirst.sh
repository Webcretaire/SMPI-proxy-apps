#!/bin/bash

rank=${1:-0}

deno run -A expected_model_change_log.ts \
    ./logs/Optimized/logS4BXI_5000/$rank.csv \
    ./logs/Optimized/logSMPI_5000/$rank.csv > ./logs/Optimized/S4BXI_first.csv

Rscript plotLogs.R \
    ./logs/Optimized/logS4BXI_5000/$rank.csv \
    ./logs/Optimized/logSMPI_5000/$rank.csv \
    ./logs/Optimized/logModelChangeMostlyS4BXI_5000/$rank.csv \
    ./logs/Optimized/S4BXI_first.csv \
    ./logs/Optimized/plot_S4BXI_first.png
