#!/bin/bash

source ./s4bxi_env.sh

out_prefix=tmp
input_file=src/HPL/HPL.dat.dist
patch=src/HPL/patch_hpl_optimization_S4BXI_neutral.diff
makefile=src/HPL/Make.S4BXI
sizes="20000"
ppn=4
node_number=4
out_folder="Optimized"

while getopts ":i:o:p:m:s:c:N:f:h" opt; do
  case $opt in
    h) echo "Usage: ./s4bxi_run_optimized.sh -i input_file -p patch -o out_prefix -f out_folder -m makefile -s input_size_array -c ppn -N nodes"
    echo "All the options have default values"
    exit 0
    ;;
    i) input_file="$OPTARG"
    ;;
    p) patch="$OPTARG"
    ;;
    o) out_prefix="$OPTARG"
    ;;
    m) makefile="$OPTARG"
    ;;
    s) sizes="$OPTARG"
    ;;
    c) ppn="$OPTARG"
    ;;
    N) node_number="$OPTARG"
    ;;
    f) out_folder="$OPTARG"
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    exit 1
    ;;
  esac
done

base_folder="$WORKSPACE/results/${out_folder}"
mkdir -p $base_folder
out_files="$base_folder/$out_prefix"

echo "Clean up the place"

rm -rf $WORKSPACE/Benchmarks/HPL/${out_folder}
mkdir -p $WORKSPACE/Benchmarks/HPL/${out_folder}
cd $WORKSPACE/Benchmarks/HPL || exit 1
echo "Download the source code"
test -e hpl-2.2.tar.gz || curl -o hpl-2.2.tar.gz -Lkf http://www.netlib.org/benchmark/hpl/hpl-2.2.tar.gz

echo "Unpack the code"
tar -xf hpl-2.2.tar.gz -C ${out_folder}
mv ${out_folder}/hpl-2.2/* ${out_folder}
rmdir ${out_folder}/hpl-2.2

echo "Install the modified sources"
cd ${out_folder} || exit 1
cp $WORKSPACE/$makefile ./Make.S4BXI
mkdir -p bin/S4BXI
cp $WORKSPACE/src/HPL/*.py bin/S4BXI
cp $WORKSPACE/src/HPL/dahu.xml bin/S4BXI/
cp $WORKSPACE/src/HPL/dgemm_model.c src/blas/
patch -p1 < $WORKSPACE/$patch

echo "Compile it"
sed -ri "s|TOPdir\s*=.+|TOPdir="`pwd`"|g" Make.S4BXI
make -s S4BXI_INSTALL_ROOT=${S4BXI_INSTALL_ROOT} startup arch=S4BXI 1> startup.log 2>&1 || exit 1
make -s SMPI_OPTS="-DSMPI_OPTIMIZATION -DSMPI_SEED=105" S4BXI_INSTALL_ROOT=${S4BXI_INSTALL_ROOT} arch=S4BXI 1> build.log 2>&1 || exit 2

echo "Compile platforms"
make_platf() {
    platf_folder=$1/build
    rm -rf $platf_folder/*
    mkdir -p $platf_folder
    (
        cd $platf_folder || exit 1
        (cmake .. -DSimGrid_PATH="${S4BXI_INSTALL_ROOT}/simgrid" && make) || exit 4
    )
}
make_platf $PLATFORMDIR/vix || exit 4
make_platf $PLATFORMDIR/amour || exit 4
make_platf $PLATFORMDIR/two_routers || exit 4

cd bin/S4BXI || exit 1
for in_size in $sizes; do
    echo "Generate input file"
    sed -e "s/#%PROBLEM_SIZE%#/${in_size}/g" \
        -e "s/#%PGRID_P%#/${node_number}/g" \
        -e "s/#%PGRID_Q%#/${ppn}/g" \
        $WORKSPACE/$input_file > ./HPL.dat

    echo "Generate deploy"
    if command -v deno &> /dev/null
    then
        deno run $WORKSPACE/s4bxi-config-generator/genDeploy.ts \
            -n amour1..${node_number} \
            -p ${ppn} \
            -i 1 \
            -t 1 > ./deploy.xml
    else
        ~/S4BXI_genDeploy_Linux \
            -n amour1..${node_number} \
            -p ${ppn} \
            -i 1 \
            -t 1 > ./deploy.xml
    fi

    #--cfg=smpi/shared-malloc-blocksize:2097152
    echo "Run app with out file ${out_files}_${in_size}.*"
    (time s4bximain $PLATFORMDIR/amour/build/libamour.so \
        ./deploy.xml \
        ./xhpl xhpl \
        --cfg=smpi/cpu-threshold:${S4BXI_CPU_THRESHOLD} \
        --cfg=smpi/host-speed:$(bc <<< "10000 * ${S4BXI_CPU_FACTOR}")Mf \
        --cfg=smpi/shared-malloc-blocksize:$(bc <<< "4096 * 0.0256 * $in_size") \
        --cfg=smpi/display-timing:yes \
    ) > "${out_files}_${in_size}.txt" 2> "${out_files}_${in_size}.log"
done

cd $WORKSPACE || exit 1

./s4bxi_parse_logs.sh ${out_folder} "${out_prefix}_${in_size}" $(( $node_number * $ppn ))
