#!/bin/zsh

results=$1
compare_data=$2
nodes=$3
ppn=$4
tasks=$(( $nodes * $ppn ))
result_prefix="$results/__${nodes}nodes_${ppn}ppn"
compare_data_prefix="$compare_data/${tasks}tasks_${ppn}ppn"

deno run -A ./hplToCsv.ts $results/*_${nodes}nodes_${ppn}ppn_*.txt > ${result_prefix}.csv
deno run -A ./hplToCsv.ts $compare_data_prefix/*.txt > $compare_data_prefix.csv

Rscript plot.R ${result_prefix}.csv $compare_data_prefix.csv ${result_prefix}.png
Rscript plot_chap_5.R ${result_prefix}.csv $compare_data_prefix.csv ${result_prefix}_chap_5.png