#!/bin/bash

rank=${1:-0}

deno run -A expected_model_change_log.ts \
    ./logs/Optimized/logS4BXI_5000/$rank.csv \
    ./logs/Optimized/logSMPI_5000/$rank.csv > ./logs/Optimized/S4BXI_first.csv

deno run -A align_logs.ts ./logs/Optimized/logS4BXI_5000/$rank.csv > ./logs/Optimized/aligned_S4BXI.csv
deno run -A align_logs.ts ./logs/Optimized/logSMPI_5000/$rank.csv > ./logs/Optimized/aligned_SMPI.csv
deno run -A align_logs.ts ./logs/Optimized/logModelChangeMostlyS4BXI_5000/$rank.csv > ./logs/Optimized/aligned_ModelChangeMostlyS4BXI.csv
deno run -A align_logs.ts ./logs/Optimized/S4BXI_first.csv > ./logs/Optimized/aligned_S4BXI_first.csv

Rscript plotLogs.R \
    ./logs/Optimized/aligned_S4BXI.csv \
    ./logs/Optimized/aligned_SMPI.csv \
    ./logs/Optimized/aligned_ModelChangeMostlyS4BXI.csv \
    ./logs/Optimized/aligned_S4BXI_first.csv \
    ./logs/Optimized/plot_aligned_S4BXI_first.png
