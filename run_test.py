'''
Run an SMPI simulation twice, once in "online" mode to generate a trace, then in "offline" mode using this trace.
The simulated times of both simulations are compared. An error is thrown if either simulation fails or if the times do
not match.

Syntax:
python run_test.py <nb_ranks> <host_file> <platform_file> <program args>

Example:
python $WORKSPACE/run_test.py 16 $PLATFORMDIR/cluster_hostfile.txt $PLATFORMDIR/cluster_crossbar.xml ./xhpl
'''

import subprocess
import sys
import logging
import argparse
import os
import shutil
import re
import time
import signal
import platform

TRACE_FILE = '/tmp/SMPI_trace'
TIMEOUT = 60*20
NA = float('NaN')

# Logging code, taken from https://stackoverflow.com/a/56944256/4110059
class CustomFormatter(logging.Formatter):
    grey = "\x1b[38;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    green = "\x1b[32;20m"
    bold_green = "\x1b[32;1m"
    reset = "\x1b[0m"
    # format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s (%(filename)s:%(lineno)d)"
    format_str = "%(asctime)s - %(levelname)s - %(message)s"
    FORMATS = {
        logging.DEBUG: grey + format_str + reset,
        logging.INFO: green + format_str + reset,
        logging.WARNING: yellow + format_str + reset,
        logging.ERROR: red + format_str + reset,
        logging.CRITICAL: bold_red + format_str + reset
    }

    @staticmethod
    def supports_color():
        '''From https://stackoverflow.com/a/22254892/4110059'''
        plat = sys.platform
        supported_platform = plat != 'Pocket PC' and (plat != 'win32' or 'ANSICON' in os.environ)
        is_a_tty = hasattr(sys.stdout, 'isatty') and sys.stdout.isatty()
        return supported_platform and is_a_tty

    def format(self, record):
        if self.supports_color():
            log_fmt = self.FORMATS.get(record.levelno)
        else:
            log_fmt = self.format_str
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)

class SimulationError(Exception):
    pass

logger = logging.getLogger("SMPI test")
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(CustomFormatter())
logger.addHandler(ch)

def get_simtime(output):
    regex = re.compile('Simulated time: (?P<number>-?\ *[0-9]+\.?[0-9]*(?:[Ee]\ *-?\ *[0-9]+)?)')
    for line in output.split('\n'):
        match = regex.search(line)
        if match:
            return float(match.group('number'))
    logger.critical('Could not find simulation time in output')
    logger.debug(output)
    raise SimulationError

def run_simulation(nb_hosts, hostfile, platformfile, *args):
    cmd = ['smpirun',  '--cfg=smpi/display-timing:yes', '-np',  str(nb_hosts),  '-hostfile',  hostfile,
            '-platform', platformfile, *args]
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, start_new_session=True)
    timeout = False
    try:
        stdout, stderr = proc.communicate(timeout=TIMEOUT)
    except subprocess.TimeoutExpired:
        logger.error('TIMEOUT - killing the simulation')
        os.killpg(os.getpgid(proc.pid), signal.SIGTERM)
        timeout = True
        stdout, stderr = proc.communicate()
    stdout = stdout.decode().strip()
    stderr = stderr.decode().strip()
    if proc.returncode != 0 or timeout:
        logger.error(f'With command {" ".join(cmd)}')
        if stdout:
            logger.debug(stdout)
        if stderr:
            logger.debug(stderr)
        raise SimulationError
    if 'warning' in stdout.lower():
        logger.warning('')
        logger.debug(stdout)
    if 'warning' in stderr.lower():
        logger.warning('')
        logger.debug(stderr)
    t = get_simtime(stderr)
    return t

def run_online_simulation(nb_hosts, hostfile, platformfile, *args):
    args = ['-trace-ti', f'--cfg=tracing/filename:{TRACE_FILE}', *args]
    t = run_simulation(nb_hosts, hostfile, platformfile, *args)
    logger.info('Online simulation:  SUCCESS')
    return t

def remove_trace_files(main_file):
    with open(main_file) as f:
        files = [l.strip() for l in f.readlines()]
    if len(files) > 0:
        dirname = os.path.dirname(files[0])
        shutil.rmtree(dirname)
    os.remove(main_file)

def run_offline_simulation(nb_hosts, hostfile, platformfile):
    args = ['-replay', TRACE_FILE]
    t = run_simulation(nb_hosts, hostfile, platformfile, *args)
    logger.info('Offline simulation: SUCCESS')
    return t

def line_count(filename):
    '''From https://stackoverflow.com/a/43179213/4110059'''
    return int(subprocess.check_output(['wc', '-l', filename]).split()[0])

def trace_size():
    with open(TRACE_FILE) as f:
        files = [l.strip() for l in f]
    return sum(line_count(f) for f in files)

def main(nb_hosts, hostfile, platformfile, *args):
    error = False
    LIMIT_RATIO = 1e-3  # let's accept a 0.1% difference, maybe this needs to be changed
    start_on = time.time()
    t = {}
    t['simulated_online'] = run_online_simulation(nb_hosts, hostfile, platformfile, *args)
    start_off = time.time()
    t['simulation_online'] = start_off - start_on
    try:
        t['simulated_offline'] = run_offline_simulation(nb_hosts, hostfile, platformfile)
    except SimulationError:
        error = True
        t['simulated_offline'] = NA
    t['simulation_offline'] = time.time() - start_off
    ratio = t['simulated_offline']/t['simulated_online']
    WIDTH = 15
    for key, val in t.items():
        t[key] = f'{val:.6f}'.rjust(WIDTH)
    lineno = f'{trace_size():,}'.rjust(2*WIDTH)
    logger.debug(f'                    | {"Online".rjust(WIDTH)} | {"Offline".rjust(WIDTH)} |')
    logger.debug(f'Simulation time (s) | {t["simulation_online"]} | {t["simulation_offline"]} |')
    logger.debug(f'Simulated time  (s) | {t["simulated_online"]} | {t["simulated_offline"]} |   {(1-ratio)*100:.2f}% difference')
    logger.debug(f'Trace size (#lines) |    {lineno} |')
    if error:
        raise SimulationError

    if abs(1-ratio) > LIMIT_RATIO:
        logger.warning(f'Different times for online and offline simulation')
        if abs(1-ratio) > LIMIT_RATIO*100:
            logger.error('Time difference is way too large, there must be a problem.')
            raise SimulationError


if __name__ == '__main__':
    logger.debug(f'Python version {platform.python_version()}')
    parser = argparse.ArgumentParser(description='Test runner for SMPI proxy apps')
    parser.add_argument('nb_hosts', type=int)
    parser.add_argument('hostfile', type=str)
    parser.add_argument('platformfile', type=str)
    parser.add_argument('program_args', type=str, nargs='*')
    args = parser.parse_args()
    pargs = sum([c.split() for c in args.program_args], [])
    try:
        main(args.nb_hosts, args.hostfile, args.platformfile, *pargs)
    except SimulationError:
        retcode = 1
    else:
        retcode = 0
    remove_trace_files(TRACE_FILE)
    sys.exit(retcode)
