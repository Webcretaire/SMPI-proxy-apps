#!/bin/bash

ppn=${1:-4}
nodes=${2:-4}
sizes=${3:-10000}
out_folder=${4}

echo "Run tests of size ${sizes} with ${ppn} ppn on ${nodes} nodes"

./smpi_run_vanilla.sh \
    -s "$sizes" \
    -f "$out_folder" \
    -o "smpirun_${nodes}nodes_${ppn}ppn" \
    -m src/HPL/Make.SMPI.amour \
    -i src/HPL/HPL.dat.144.dist \
    -c ${ppn} -N ${nodes}
S4BXI_SMPI_IMPLEM=0 ./s4bxi_run_vanilla.sh \
    -p src/HPL/patch_hpl_vanilla_S4BXI_model_change.diff \
    -s "$sizes" \
    -f "$out_folder" \
    -o "modelChange_${nodes}nodes_${ppn}ppn" \
    -m src/HPL/Make.S4BXI.amour \
    -i src/HPL/HPL.dat.144.dist \
    -c ${ppn} -N ${nodes}
S4BXI_SMPI_IMPLEM=1 ./s4bxi_run_vanilla.sh \
    -s "$sizes" \
    -f "$out_folder" \
    -o "smpi_${nodes}nodes_${ppn}ppn" \
    -m src/HPL/Make.S4BXI.amour \
    -i src/HPL/HPL.dat.144.dist \
    -c ${ppn} -N ${nodes}
S4BXI_SMPI_IMPLEM=0 ./s4bxi_run_vanilla.sh \
    -s "$sizes" \
    -f "$out_folder" \
    -o "s4bxi_${nodes}nodes_${ppn}ppn" \
    -m src/HPL/Make.S4BXI.amour \
    -i src/HPL/HPL.dat.144.dist \
    -c ${ppn} -N ${nodes}
