#!/bin/zsh

results=$1
compare_data=$2

for i in $( seq 1 8 ); do
    ./plot.sh $1 $2 $(( 2 * $i )) $i
done