================================================================================
HPLinpack 2.2  --  High-Performance Linpack benchmark  --   February 24, 2016
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :    2500 
NB     :     120 
PMAP   : Row-major process mapping
P      :       4 
Q      :       4 
PFACT  :    Left 
NBMIN  :       2 
NDIV   :       2 
RFACT  :    Left 
BCAST  :   1ring 
DEPTH  :       0 
SWAP   : Mix (threshold = 64)
L1     : transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

Column=000000120 Fraction= 4.8% Gflops=1.296e+02
Column=000000240 Fraction= 9.6% Gflops=1.255e+02
Column=000000360 Fraction=14.4% Gflops=1.219e+02
Column=000000480 Fraction=19.2% Gflops=1.173e+02
Column=000000600 Fraction=24.0% Gflops=1.162e+02
Column=000000720 Fraction=28.8% Gflops=1.127e+02
Column=000000840 Fraction=33.6% Gflops=1.090e+02
Column=000000960 Fraction=38.4% Gflops=1.053e+02
Column=000001080 Fraction=43.2% Gflops=1.029e+02
Column=000001200 Fraction=48.0% Gflops=9.960e+01
Column=000001320 Fraction=52.8% Gflops=9.629e+01
Column=000001440 Fraction=57.6% Gflops=9.301e+01
Column=000001560 Fraction=62.4% Gflops=9.041e+01
Column=000001680 Fraction=67.2% Gflops=8.740e+01
Column=000001800 Fraction=72.0% Gflops=8.440e+01
Column=000001920 Fraction=76.8% Gflops=8.146e+01
Column=000002040 Fraction=81.6% Gflops=7.890e+01
Column=000002160 Fraction=86.4% Gflops=7.622e+01
Column=000002280 Fraction=91.2% Gflops=7.373e+01
Column=000002400 Fraction=96.0% Gflops=7.134e+01
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00L2L2        2500   120     4     4               0.15              6.904e+01
HPL_pdgesv() start time Mon May 30 18:24:49 2022

HPL_pdgesv() end time   Mon May 30 18:24:54 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0054275 ...... PASSED
================================================================================

Finished      1 tests with the following results:
              1 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
