================================================================================
HPLinpack 2.2  --  High-Performance Linpack benchmark  --   February 24, 2016
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   10000 
NB     :     120 
PMAP   : Row-major process mapping
P      :       4 
Q      :       4 
PFACT  :    Left 
NBMIN  :       2 
NDIV   :       2 
RFACT  :    Left 
BCAST  :   1ring 
DEPTH  :       0 
SWAP   : Mix (threshold = 64)
L1     : transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

Column=000000120 Fraction= 1.2% Gflops=2.886e+02
Column=000000240 Fraction= 2.4% Gflops=2.858e+02
Column=000000360 Fraction= 3.6% Gflops=2.826e+02
Column=000000480 Fraction= 4.8% Gflops=2.827e+02
Column=000000600 Fraction= 6.0% Gflops=2.891e+02
Column=000000720 Fraction= 7.2% Gflops=2.850e+02
Column=000000840 Fraction= 8.4% Gflops=2.811e+02
Column=000000960 Fraction= 9.6% Gflops=2.810e+02
Column=000001080 Fraction=10.8% Gflops=2.833e+02
Column=000001200 Fraction=12.0% Gflops=2.831e+02
Column=000001320 Fraction=13.2% Gflops=2.823e+02
Column=000001440 Fraction=14.4% Gflops=2.821e+02
Column=000001560 Fraction=15.6% Gflops=2.834e+02
Column=000001680 Fraction=16.8% Gflops=2.824e+02
Column=000001800 Fraction=18.0% Gflops=2.812e+02
Column=000001920 Fraction=19.2% Gflops=2.804e+02
Column=000002040 Fraction=20.4% Gflops=2.781e+02
Column=000002160 Fraction=21.6% Gflops=2.775e+02
Column=000002280 Fraction=22.8% Gflops=2.765e+02
Column=000002400 Fraction=24.0% Gflops=2.753e+02
Column=000002520 Fraction=25.2% Gflops=2.760e+02
Column=000002640 Fraction=26.4% Gflops=2.731e+02
Column=000002760 Fraction=27.6% Gflops=2.701e+02
Column=000002880 Fraction=28.8% Gflops=2.700e+02
Column=000003000 Fraction=30.0% Gflops=2.704e+02
Column=000003120 Fraction=31.2% Gflops=2.700e+02
Column=000003240 Fraction=32.4% Gflops=2.694e+02
Column=000003360 Fraction=33.6% Gflops=2.692e+02
Column=000003480 Fraction=34.8% Gflops=2.696e+02
Column=000003600 Fraction=36.0% Gflops=2.690e+02
Column=000003720 Fraction=37.2% Gflops=2.683e+02
Column=000003840 Fraction=38.4% Gflops=2.677e+02
Column=000003960 Fraction=39.6% Gflops=2.677e+02
Column=000004080 Fraction=40.8% Gflops=2.672e+02
Column=000004200 Fraction=42.0% Gflops=2.665e+02
Column=000004320 Fraction=43.2% Gflops=2.660e+02
Column=000004440 Fraction=44.4% Gflops=2.661e+02
Column=000004560 Fraction=45.6% Gflops=2.653e+02
Column=000004680 Fraction=46.8% Gflops=2.644e+02
Column=000004800 Fraction=48.0% Gflops=2.640e+02
Column=000004920 Fraction=49.2% Gflops=2.639e+02
Column=000005040 Fraction=50.4% Gflops=2.633e+02
Column=000005160 Fraction=51.6% Gflops=2.627e+02
Column=000005280 Fraction=52.8% Gflops=2.623e+02
Column=000005400 Fraction=54.0% Gflops=2.622e+02
Column=000005520 Fraction=55.2% Gflops=2.615e+02
Column=000005640 Fraction=56.4% Gflops=2.608e+02
Column=000005760 Fraction=57.6% Gflops=2.603e+02
Column=000005880 Fraction=58.8% Gflops=2.601e+02
Column=000006000 Fraction=60.0% Gflops=2.594e+02
Column=000006120 Fraction=61.2% Gflops=2.588e+02
Column=000006240 Fraction=62.4% Gflops=2.583e+02
Column=000006360 Fraction=63.6% Gflops=2.580e+02
Column=000006480 Fraction=64.8% Gflops=2.574e+02
Column=000006600 Fraction=66.0% Gflops=2.568e+02
Column=000006720 Fraction=67.2% Gflops=2.562e+02
Column=000006840 Fraction=68.4% Gflops=2.559e+02
Column=000006960 Fraction=69.6% Gflops=2.553e+02
Column=000007080 Fraction=70.8% Gflops=2.546e+02
Column=000007200 Fraction=72.0% Gflops=2.541e+02
Column=000007320 Fraction=73.2% Gflops=2.537e+02
Column=000007440 Fraction=74.4% Gflops=2.531e+02
Column=000007560 Fraction=75.6% Gflops=2.525e+02
Column=000007680 Fraction=76.8% Gflops=2.520e+02
Column=000007800 Fraction=78.0% Gflops=2.516e+02
Column=000007920 Fraction=79.2% Gflops=2.510e+02
Column=000008040 Fraction=80.4% Gflops=2.505e+02
Column=000008160 Fraction=81.6% Gflops=2.499e+02
Column=000008280 Fraction=82.8% Gflops=2.495e+02
Column=000008400 Fraction=84.0% Gflops=2.490e+02
Column=000008520 Fraction=85.2% Gflops=2.484e+02
Column=000008640 Fraction=86.4% Gflops=2.479e+02
Column=000008760 Fraction=87.6% Gflops=2.475e+02
Column=000008880 Fraction=88.8% Gflops=2.470e+02
Column=000009000 Fraction=90.0% Gflops=2.465e+02
Column=000009120 Fraction=91.2% Gflops=2.460e+02
Column=000009240 Fraction=92.4% Gflops=2.456e+02
Column=000009360 Fraction=93.6% Gflops=2.451e+02
Column=000009480 Fraction=94.8% Gflops=2.446e+02
Column=000009600 Fraction=96.0% Gflops=2.442e+02
Column=000009720 Fraction=97.2% Gflops=2.438e+02
Column=000009840 Fraction=98.4% Gflops=2.433e+02
Column=000009960 Fraction=99.6% Gflops=2.429e+02
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00L2L2       10000   120     4     4               2.75              2.423e+02
HPL_pdgesv() start time Mon May 30 18:25:22 2022

HPL_pdgesv() end time   Mon May 30 18:26:08 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0043843 ...... PASSED
================================================================================

Finished      1 tests with the following results:
              1 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
