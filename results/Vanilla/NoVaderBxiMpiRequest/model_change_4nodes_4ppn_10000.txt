================================================================================
HPLinpack 2.2  --  High-Performance Linpack benchmark  --   February 24, 2016
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   10000 
NB     :     120 
PMAP   : Row-major process mapping
P      :       4 
Q      :       4 
PFACT  :    Left 
NBMIN  :       2 
NDIV   :       2 
RFACT  :    Left 
BCAST  :   1ring 
DEPTH  :       0 
SWAP   : Mix (threshold = 64)
L1     : transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

Column=000000120 Fraction= 1.2% Gflops=2.371e+07
Column=000000240 Fraction= 2.4% Gflops=4.686e+07
Column=000000360 Fraction= 3.6% Gflops=6.944e+07
Column=000000480 Fraction= 4.8% Gflops=2.293e+03
Column=000000600 Fraction= 6.0% Gflops=1.507e+03
Column=000000720 Fraction= 7.2% Gflops=1.157e+03
Column=000000840 Fraction= 8.4% Gflops=9.843e+02
Column=000000960 Fraction= 9.6% Gflops=8.995e+02
Column=000001080 Fraction=10.8% Gflops=8.546e+02
Column=000001200 Fraction=12.0% Gflops=8.121e+02
Column=000001320 Fraction=13.2% Gflops=7.820e+02
Column=000001440 Fraction=14.4% Gflops=7.613e+02
Column=000001560 Fraction=15.6% Gflops=7.497e+02
Column=000001680 Fraction=16.8% Gflops=7.304e+02
Column=000001800 Fraction=18.0% Gflops=7.148e+02
Column=000001920 Fraction=19.2% Gflops=7.033e+02
Column=000002040 Fraction=20.4% Gflops=6.876e+02
Column=000002160 Fraction=21.6% Gflops=6.779e+02
Column=000002280 Fraction=22.8% Gflops=6.682e+02
Column=000002400 Fraction=24.0% Gflops=6.587e+02
Column=000002520 Fraction=25.2% Gflops=6.552e+02
Column=000002640 Fraction=26.4% Gflops=6.416e+02
Column=000002760 Fraction=27.6% Gflops=6.293e+02
Column=000002880 Fraction=28.8% Gflops=6.252e+02
Column=000003000 Fraction=30.0% Gflops=6.225e+02
Column=000003120 Fraction=31.2% Gflops=6.180e+02
Column=000003240 Fraction=32.4% Gflops=6.132e+02
Column=000003360 Fraction=33.6% Gflops=6.103e+02
Column=000003480 Fraction=34.8% Gflops=6.089e+02
Column=000003600 Fraction=36.0% Gflops=6.050e+02
Column=000003720 Fraction=37.2% Gflops=6.017e+02
Column=000003840 Fraction=38.4% Gflops=5.993e+02
Column=000003960 Fraction=39.6% Gflops=5.982e+02
Column=000004080 Fraction=40.8% Gflops=5.956e+02
Column=000004200 Fraction=42.0% Gflops=5.931e+02
Column=000004320 Fraction=43.2% Gflops=5.913e+02
Column=000004440 Fraction=44.4% Gflops=5.907e+02
Column=000004560 Fraction=45.6% Gflops=5.878e+02
Column=000004680 Fraction=46.8% Gflops=5.851e+02
Column=000004800 Fraction=48.0% Gflops=5.832e+02
Column=000004920 Fraction=49.2% Gflops=5.818e+02
Column=000005040 Fraction=50.4% Gflops=5.793e+02
Column=000005160 Fraction=51.6% Gflops=5.767e+02
Column=000005280 Fraction=52.8% Gflops=5.747e+02
Column=000005400 Fraction=54.0% Gflops=5.736e+02
Column=000005520 Fraction=55.2% Gflops=5.711e+02
Column=000005640 Fraction=56.4% Gflops=5.686e+02
Column=000005760 Fraction=57.6% Gflops=5.665e+02
Column=000005880 Fraction=58.8% Gflops=5.652e+02
Column=000006000 Fraction=60.0% Gflops=5.631e+02
Column=000006120 Fraction=61.2% Gflops=5.613e+02
Column=000006240 Fraction=62.4% Gflops=5.601e+02
Column=000006360 Fraction=63.6% Gflops=5.593e+02
Column=000006480 Fraction=64.8% Gflops=5.576e+02
Column=000006600 Fraction=66.0% Gflops=5.559e+02
Column=000006720 Fraction=67.2% Gflops=5.546e+02
Column=000006840 Fraction=68.4% Gflops=5.538e+02
Column=000006960 Fraction=69.6% Gflops=5.522e+02
Column=000007080 Fraction=70.8% Gflops=5.507e+02
Column=000007200 Fraction=72.0% Gflops=5.493e+02
Column=000007320 Fraction=73.2% Gflops=5.481e+02
Column=000007440 Fraction=74.4% Gflops=5.463e+02
Column=000007560 Fraction=75.6% Gflops=5.446e+02
Column=000007680 Fraction=76.8% Gflops=5.431e+02
Column=000007800 Fraction=78.0% Gflops=5.419e+02
Column=000007920 Fraction=79.2% Gflops=5.403e+02
Column=000008040 Fraction=80.4% Gflops=5.386e+02
Column=000008160 Fraction=81.6% Gflops=5.372e+02
Column=000008280 Fraction=82.8% Gflops=5.360e+02
Column=000008400 Fraction=84.0% Gflops=5.345e+02
Column=000008520 Fraction=85.2% Gflops=5.333e+02
Column=000008640 Fraction=86.4% Gflops=5.323e+02
Column=000008760 Fraction=87.6% Gflops=5.315e+02
Column=000008880 Fraction=88.8% Gflops=5.304e+02
Column=000009000 Fraction=90.0% Gflops=5.293e+02
Column=000009120 Fraction=91.2% Gflops=5.283e+02
Column=000009240 Fraction=92.4% Gflops=5.276e+02
Column=000009360 Fraction=93.6% Gflops=5.266e+02
Column=000009480 Fraction=94.8% Gflops=5.256e+02
Column=000009600 Fraction=96.0% Gflops=5.248e+02
Column=000009720 Fraction=97.2% Gflops=5.237e+02
Column=000009840 Fraction=98.4% Gflops=5.226e+02
Column=000009960 Fraction=99.6% Gflops=5.215e+02
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00L2L2       10000   120     4     4               1.41              4.726e+02
HPL_pdgesv() start time Fri May 27 18:37:57 2022

HPL_pdgesv() end time   Fri May 27 18:38:34 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0043843 ...... PASSED
================================================================================

Finished      1 tests with the following results:
              1 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
