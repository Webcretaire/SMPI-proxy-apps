================================================================================
HPLinpack 2.2  --  High-Performance Linpack benchmark  --   February 24, 2016
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   20000 
NB     :     120 
PMAP   : Row-major process mapping
P      :       4 
Q      :       4 
PFACT  :    Left 
NBMIN  :       2 
NDIV   :       2 
RFACT  :    Left 
BCAST  :   1ring 
DEPTH  :       0 
SWAP   : Mix (threshold = 64)
L1     : transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

Column=000000120 Fraction= 0.6% Gflops=6.807e+02
Column=000000240 Fraction= 1.2% Gflops=6.454e+02
Column=000000360 Fraction= 1.8% Gflops=6.482e+02
Column=000000480 Fraction= 2.4% Gflops=6.450e+02
Column=000000600 Fraction= 3.0% Gflops=6.069e+02
Column=000000720 Fraction= 3.6% Gflops=5.784e+02
Column=000000840 Fraction= 4.2% Gflops=5.677e+02
Column=000000960 Fraction= 4.8% Gflops=5.567e+02
Column=000001080 Fraction= 5.4% Gflops=5.652e+02
Column=000001200 Fraction= 6.0% Gflops=5.568e+02
Column=000001320 Fraction= 6.6% Gflops=5.639e+02
Column=000001440 Fraction= 7.2% Gflops=5.680e+02
Column=000001560 Fraction= 7.8% Gflops=5.729e+02
Column=000001680 Fraction= 8.4% Gflops=5.744e+02
Column=000001800 Fraction= 9.0% Gflops=5.766e+02
Column=000001920 Fraction= 9.6% Gflops=5.801e+02
Column=000002040 Fraction=10.2% Gflops=5.843e+02
Column=000002160 Fraction=10.8% Gflops=5.859e+02
Column=000002280 Fraction=11.4% Gflops=5.887e+02
Column=000002400 Fraction=12.0% Gflops=5.903e+02
Column=000002520 Fraction=12.6% Gflops=5.909e+02
Column=000002640 Fraction=13.2% Gflops=5.911e+02
Column=000002760 Fraction=13.8% Gflops=5.915e+02
Column=000002880 Fraction=14.4% Gflops=5.919e+02
Column=000003000 Fraction=15.0% Gflops=5.942e+02
Column=000003120 Fraction=15.6% Gflops=5.938e+02
Column=000003240 Fraction=16.2% Gflops=5.949e+02
Column=000003360 Fraction=16.8% Gflops=5.954e+02
Column=000003480 Fraction=17.4% Gflops=5.961e+02
Column=000003600 Fraction=18.0% Gflops=5.961e+02
Column=000003720 Fraction=18.6% Gflops=5.961e+02
Column=000003840 Fraction=19.2% Gflops=5.970e+02
Column=000003960 Fraction=19.8% Gflops=5.987e+02
Column=000004080 Fraction=20.4% Gflops=5.989e+02
Column=000004200 Fraction=21.0% Gflops=5.998e+02
Column=000004320 Fraction=21.6% Gflops=6.004e+02
Column=000004440 Fraction=22.2% Gflops=5.969e+02
Column=000004560 Fraction=22.8% Gflops=5.935e+02
Column=000004680 Fraction=23.4% Gflops=5.914e+02
Column=000004800 Fraction=24.0% Gflops=5.887e+02
Column=000004920 Fraction=24.6% Gflops=5.899e+02
Column=000005040 Fraction=25.2% Gflops=5.871e+02
Column=000005160 Fraction=25.8% Gflops=5.877e+02
Column=000005280 Fraction=26.4% Gflops=5.880e+02
Column=000005400 Fraction=27.0% Gflops=5.881e+02
Column=000005520 Fraction=27.6% Gflops=5.881e+02
Column=000005640 Fraction=28.2% Gflops=5.880e+02
Column=000005760 Fraction=28.8% Gflops=5.882e+02
Column=000005880 Fraction=29.4% Gflops=5.891e+02
Column=000006000 Fraction=30.0% Gflops=5.890e+02
Column=000006120 Fraction=30.6% Gflops=5.894e+02
Column=000006240 Fraction=31.2% Gflops=5.899e+02
Column=000006360 Fraction=31.8% Gflops=5.900e+02
Column=000006480 Fraction=32.4% Gflops=5.900e+02
Column=000006600 Fraction=33.0% Gflops=5.900e+02
Column=000006720 Fraction=33.6% Gflops=5.901e+02
Column=000006840 Fraction=34.2% Gflops=5.907e+02
Column=000006960 Fraction=34.8% Gflops=5.904e+02
Column=000007080 Fraction=35.4% Gflops=5.906e+02
Column=000007200 Fraction=36.0% Gflops=5.907e+02
Column=000007320 Fraction=36.6% Gflops=5.907e+02
Column=000007440 Fraction=37.2% Gflops=5.904e+02
Column=000007560 Fraction=37.8% Gflops=5.901e+02
Column=000007680 Fraction=38.4% Gflops=5.902e+02
Column=000007800 Fraction=39.0% Gflops=5.906e+02
Column=000007920 Fraction=39.6% Gflops=5.903e+02
Column=000008040 Fraction=40.2% Gflops=5.901e+02
Column=000008160 Fraction=40.8% Gflops=5.900e+02
Column=000008280 Fraction=41.4% Gflops=5.890e+02
Column=000008400 Fraction=42.0% Gflops=5.882e+02
Column=000008520 Fraction=42.6% Gflops=5.876e+02
Column=000008640 Fraction=43.2% Gflops=5.867e+02
Column=000008760 Fraction=43.8% Gflops=5.871e+02
Column=000008880 Fraction=44.4% Gflops=5.859e+02
Column=000009000 Fraction=45.0% Gflops=5.859e+02
Column=000009120 Fraction=45.6% Gflops=5.860e+02
Column=000009240 Fraction=46.2% Gflops=5.860e+02
Column=000009360 Fraction=46.8% Gflops=5.858e+02
Column=000009480 Fraction=47.4% Gflops=5.855e+02
Column=000009600 Fraction=48.0% Gflops=5.856e+02
Column=000009720 Fraction=48.6% Gflops=5.858e+02
Column=000009840 Fraction=49.2% Gflops=5.855e+02
Column=000009960 Fraction=49.8% Gflops=5.853e+02
Column=000010080 Fraction=50.4% Gflops=5.852e+02
Column=000010200 Fraction=51.0% Gflops=5.851e+02
Column=000010320 Fraction=51.6% Gflops=5.848e+02
Column=000010440 Fraction=52.2% Gflops=5.846e+02
Column=000010560 Fraction=52.8% Gflops=5.844e+02
Column=000010680 Fraction=53.4% Gflops=5.845e+02
Column=000010800 Fraction=54.0% Gflops=5.841e+02
Column=000010920 Fraction=54.6% Gflops=5.839e+02
Column=000011040 Fraction=55.2% Gflops=5.839e+02
Column=000011160 Fraction=55.8% Gflops=5.838e+02
Column=000011280 Fraction=56.4% Gflops=5.836e+02
Column=000011400 Fraction=57.0% Gflops=5.834e+02
Column=000011520 Fraction=57.6% Gflops=5.833e+02
Column=000011640 Fraction=58.2% Gflops=5.834e+02
Column=000011760 Fraction=58.8% Gflops=5.831e+02
Column=000011880 Fraction=59.4% Gflops=5.828e+02
Column=000012000 Fraction=60.0% Gflops=5.827e+02
Column=000012120 Fraction=60.6% Gflops=5.823e+02
Column=000012240 Fraction=61.2% Gflops=5.820e+02
Column=000012360 Fraction=61.8% Gflops=5.816e+02
Column=000012480 Fraction=62.4% Gflops=5.810e+02
Column=000012600 Fraction=63.0% Gflops=5.809e+02
Column=000012720 Fraction=63.6% Gflops=5.802e+02
Column=000012840 Fraction=64.2% Gflops=5.800e+02
Column=000012960 Fraction=64.8% Gflops=5.798e+02
Column=000013080 Fraction=65.4% Gflops=5.796e+02
Column=000013200 Fraction=66.0% Gflops=5.793e+02
Column=000013320 Fraction=66.6% Gflops=5.791e+02
Column=000013440 Fraction=67.2% Gflops=5.790e+02
Column=000013560 Fraction=67.8% Gflops=5.789e+02
Column=000013680 Fraction=68.4% Gflops=5.787e+02
Column=000013800 Fraction=69.0% Gflops=5.785e+02
Column=000013920 Fraction=69.6% Gflops=5.783e+02
Column=000014040 Fraction=70.2% Gflops=5.782e+02
Column=000014160 Fraction=70.8% Gflops=5.780e+02
Column=000014280 Fraction=71.4% Gflops=5.777e+02
Column=000014400 Fraction=72.0% Gflops=5.775e+02
Column=000014520 Fraction=72.6% Gflops=5.774e+02
Column=000014640 Fraction=73.2% Gflops=5.771e+02
Column=000014760 Fraction=73.8% Gflops=5.768e+02
Column=000014880 Fraction=74.4% Gflops=5.766e+02
Column=000015000 Fraction=75.0% Gflops=5.764e+02
Column=000015120 Fraction=75.6% Gflops=5.760e+02
Column=000015240 Fraction=76.2% Gflops=5.756e+02
Column=000015360 Fraction=76.8% Gflops=5.754e+02
Column=000015480 Fraction=77.4% Gflops=5.752e+02
Column=000015600 Fraction=78.0% Gflops=5.749e+02
Column=000015720 Fraction=78.6% Gflops=5.747e+02
Column=000015840 Fraction=79.2% Gflops=5.746e+02
Column=000015960 Fraction=79.8% Gflops=5.745e+02
Column=000016080 Fraction=80.4% Gflops=5.742e+02
Column=000016200 Fraction=81.0% Gflops=5.740e+02
Column=000016320 Fraction=81.6% Gflops=5.738e+02
Column=000016440 Fraction=82.2% Gflops=5.737e+02
Column=000016560 Fraction=82.8% Gflops=5.735e+02
Column=000016680 Fraction=83.4% Gflops=5.733e+02
Column=000016800 Fraction=84.0% Gflops=5.731e+02
Column=000016920 Fraction=84.6% Gflops=5.729e+02
Column=000017040 Fraction=85.2% Gflops=5.726e+02
Column=000017160 Fraction=85.8% Gflops=5.724e+02
Column=000017280 Fraction=86.4% Gflops=5.722e+02
Column=000017400 Fraction=87.0% Gflops=5.720e+02
Column=000017520 Fraction=87.6% Gflops=5.717e+02
Column=000017640 Fraction=88.2% Gflops=5.715e+02
Column=000017760 Fraction=88.8% Gflops=5.713e+02
Column=000017880 Fraction=89.4% Gflops=5.711e+02
Column=000018000 Fraction=90.0% Gflops=5.709e+02
Column=000018120 Fraction=90.6% Gflops=5.707e+02
Column=000018240 Fraction=91.2% Gflops=5.705e+02
Column=000018360 Fraction=91.8% Gflops=5.704e+02
Column=000018480 Fraction=92.4% Gflops=5.702e+02
Column=000018600 Fraction=93.0% Gflops=5.701e+02
Column=000018720 Fraction=93.6% Gflops=5.699e+02
Column=000018840 Fraction=94.2% Gflops=5.698e+02
Column=000018960 Fraction=94.8% Gflops=5.696e+02
Column=000019080 Fraction=95.4% Gflops=5.695e+02
Column=000019200 Fraction=96.0% Gflops=5.693e+02
Column=000019320 Fraction=96.6% Gflops=5.692e+02
Column=000019440 Fraction=97.2% Gflops=5.690e+02
Column=000019560 Fraction=97.8% Gflops=5.688e+02
Column=000019680 Fraction=98.4% Gflops=5.686e+02
Column=000019800 Fraction=99.0% Gflops=5.685e+02
Column=000019920 Fraction=99.6% Gflops=5.683e+02
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00L2L2       20000   120     4     4               9.39              5.679e+02
HPL_pdgesv() start time Fri May 27 18:40:40 2022

HPL_pdgesv() end time   Fri May 27 18:44:02 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0034198 ...... PASSED
================================================================================

Finished      1 tests with the following results:
              1 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
