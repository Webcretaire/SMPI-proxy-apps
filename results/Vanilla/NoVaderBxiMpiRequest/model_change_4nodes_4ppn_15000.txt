================================================================================
HPLinpack 2.2  --  High-Performance Linpack benchmark  --   February 24, 2016
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   15000 
NB     :     120 
PMAP   : Row-major process mapping
P      :       4 
Q      :       4 
PFACT  :    Left 
NBMIN  :       2 
NDIV   :       2 
RFACT  :    Left 
BCAST  :   1ring 
DEPTH  :       0 
SWAP   : Mix (threshold = 64)
L1     : transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

Column=000000120 Fraction= 0.8% Gflops=6.266e+02
Column=000000240 Fraction= 1.6% Gflops=6.259e+02
Column=000000360 Fraction= 2.4% Gflops=6.227e+02
Column=000000480 Fraction= 3.2% Gflops=6.209e+02
Column=000000600 Fraction= 4.0% Gflops=6.175e+02
Column=000000720 Fraction= 4.8% Gflops=6.153e+02
Column=000000840 Fraction= 5.6% Gflops=6.123e+02
Column=000000960 Fraction= 6.4% Gflops=6.095e+02
Column=000001080 Fraction= 7.2% Gflops=6.112e+02
Column=000001200 Fraction= 8.0% Gflops=6.117e+02
Column=000001320 Fraction= 8.8% Gflops=6.115e+02
Column=000001440 Fraction= 9.6% Gflops=6.112e+02
Column=000001560 Fraction=10.4% Gflops=6.086e+02
Column=000001680 Fraction=11.2% Gflops=6.068e+02
Column=000001800 Fraction=12.0% Gflops=6.049e+02
Column=000001920 Fraction=12.8% Gflops=6.029e+02
Column=000002040 Fraction=13.6% Gflops=6.041e+02
Column=000002160 Fraction=14.4% Gflops=6.043e+02
Column=000002280 Fraction=15.2% Gflops=6.048e+02
Column=000002400 Fraction=16.0% Gflops=6.049e+02
Column=000002520 Fraction=16.8% Gflops=6.045e+02
Column=000002640 Fraction=17.6% Gflops=6.038e+02
Column=000002760 Fraction=18.4% Gflops=6.028e+02
Column=000002880 Fraction=19.2% Gflops=6.018e+02
Column=000003000 Fraction=20.0% Gflops=6.021e+02
Column=000003120 Fraction=20.8% Gflops=6.020e+02
Column=000003240 Fraction=21.6% Gflops=6.013e+02
Column=000003360 Fraction=22.4% Gflops=6.007e+02
Column=000003480 Fraction=23.2% Gflops=5.961e+02
Column=000003600 Fraction=24.0% Gflops=5.908e+02
Column=000003720 Fraction=24.8% Gflops=5.869e+02
Column=000003840 Fraction=25.6% Gflops=5.831e+02
Column=000003960 Fraction=26.4% Gflops=5.837e+02
Column=000004080 Fraction=27.2% Gflops=5.840e+02
Column=000004200 Fraction=28.0% Gflops=5.843e+02
Column=000004320 Fraction=28.8% Gflops=5.845e+02
Column=000004440 Fraction=29.6% Gflops=5.846e+02
Column=000004560 Fraction=30.4% Gflops=5.843e+02
Column=000004680 Fraction=31.2% Gflops=5.841e+02
Column=000004800 Fraction=32.0% Gflops=5.837e+02
Column=000004920 Fraction=32.8% Gflops=5.837e+02
Column=000005040 Fraction=33.6% Gflops=5.835e+02
Column=000005160 Fraction=34.4% Gflops=5.831e+02
Column=000005280 Fraction=35.2% Gflops=5.826e+02
Column=000005400 Fraction=36.0% Gflops=5.819e+02
Column=000005520 Fraction=36.8% Gflops=5.812e+02
Column=000005640 Fraction=37.6% Gflops=5.803e+02
Column=000005760 Fraction=38.4% Gflops=5.794e+02
Column=000005880 Fraction=39.2% Gflops=5.793e+02
Column=000006000 Fraction=40.0% Gflops=5.788e+02
Column=000006120 Fraction=40.8% Gflops=5.787e+02
Column=000006240 Fraction=41.6% Gflops=5.785e+02
Column=000006360 Fraction=42.4% Gflops=5.783e+02
Column=000006480 Fraction=43.2% Gflops=5.780e+02
Column=000006600 Fraction=44.0% Gflops=5.777e+02
Column=000006720 Fraction=44.8% Gflops=5.773e+02
Column=000006840 Fraction=45.6% Gflops=5.772e+02
Column=000006960 Fraction=46.4% Gflops=5.768e+02
Column=000007080 Fraction=47.2% Gflops=5.765e+02
Column=000007200 Fraction=48.0% Gflops=5.761e+02
Column=000007320 Fraction=48.8% Gflops=5.747e+02
Column=000007440 Fraction=49.6% Gflops=5.737e+02
Column=000007560 Fraction=50.4% Gflops=5.722e+02
Column=000007680 Fraction=51.2% Gflops=5.706e+02
Column=000007800 Fraction=52.0% Gflops=5.703e+02
Column=000007920 Fraction=52.8% Gflops=5.699e+02
Column=000008040 Fraction=53.6% Gflops=5.694e+02
Column=000008160 Fraction=54.4% Gflops=5.689e+02
Column=000008280 Fraction=55.2% Gflops=5.685e+02
Column=000008400 Fraction=56.0% Gflops=5.681e+02
Column=000008520 Fraction=56.8% Gflops=5.677e+02
Column=000008640 Fraction=57.6% Gflops=5.673e+02
Column=000008760 Fraction=58.4% Gflops=5.671e+02
Column=000008880 Fraction=59.2% Gflops=5.667e+02
Column=000009000 Fraction=60.0% Gflops=5.664e+02
Column=000009120 Fraction=60.8% Gflops=5.660e+02
Column=000009240 Fraction=61.6% Gflops=5.656e+02
Column=000009360 Fraction=62.4% Gflops=5.652e+02
Column=000009480 Fraction=63.2% Gflops=5.647e+02
Column=000009600 Fraction=64.0% Gflops=5.640e+02
Column=000009720 Fraction=64.8% Gflops=5.636e+02
Column=000009840 Fraction=65.6% Gflops=5.631e+02
Column=000009960 Fraction=66.4% Gflops=5.626e+02
Column=000010080 Fraction=67.2% Gflops=5.621e+02
Column=000010200 Fraction=68.0% Gflops=5.616e+02
Column=000010320 Fraction=68.8% Gflops=5.611e+02
Column=000010440 Fraction=69.6% Gflops=5.606e+02
Column=000010560 Fraction=70.4% Gflops=5.600e+02
Column=000010680 Fraction=71.2% Gflops=5.596e+02
Column=000010800 Fraction=72.0% Gflops=5.590e+02
Column=000010920 Fraction=72.8% Gflops=5.586e+02
Column=000011040 Fraction=73.6% Gflops=5.582e+02
Column=000011160 Fraction=74.4% Gflops=5.578e+02
Column=000011280 Fraction=75.2% Gflops=5.574e+02
Column=000011400 Fraction=76.0% Gflops=5.570e+02
Column=000011520 Fraction=76.8% Gflops=5.565e+02
Column=000011640 Fraction=77.6% Gflops=5.562e+02
Column=000011760 Fraction=78.4% Gflops=5.558e+02
Column=000011880 Fraction=79.2% Gflops=5.554e+02
Column=000012000 Fraction=80.0% Gflops=5.549e+02
Column=000012120 Fraction=80.8% Gflops=5.544e+02
Column=000012240 Fraction=81.6% Gflops=5.539e+02
Column=000012360 Fraction=82.4% Gflops=5.534e+02
Column=000012480 Fraction=83.2% Gflops=5.529e+02
Column=000012600 Fraction=84.0% Gflops=5.525e+02
Column=000012720 Fraction=84.8% Gflops=5.520e+02
Column=000012840 Fraction=85.6% Gflops=5.515e+02
Column=000012960 Fraction=86.4% Gflops=5.510e+02
Column=000013080 Fraction=87.2% Gflops=5.506e+02
Column=000013200 Fraction=88.0% Gflops=5.501e+02
Column=000013320 Fraction=88.8% Gflops=5.498e+02
Column=000013440 Fraction=89.6% Gflops=5.495e+02
Column=000013560 Fraction=90.4% Gflops=5.491e+02
Column=000013680 Fraction=91.2% Gflops=5.488e+02
Column=000013800 Fraction=92.0% Gflops=5.485e+02
Column=000013920 Fraction=92.8% Gflops=5.482e+02
Column=000014040 Fraction=93.6% Gflops=5.479e+02
Column=000014160 Fraction=94.4% Gflops=5.476e+02
Column=000014280 Fraction=95.2% Gflops=5.473e+02
Column=000014400 Fraction=96.0% Gflops=5.469e+02
Column=000014520 Fraction=96.8% Gflops=5.466e+02
Column=000014640 Fraction=97.6% Gflops=5.462e+02
Column=000014760 Fraction=98.4% Gflops=5.458e+02
Column=000014880 Fraction=99.2% Gflops=5.455e+02
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00L2L2       15000   120     4     4               4.13              5.446e+02
HPL_pdgesv() start time Fri May 27 18:38:47 2022

HPL_pdgesv() end time   Fri May 27 18:40:21 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0036873 ...... PASSED
================================================================================

Finished      1 tests with the following results:
              1 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
