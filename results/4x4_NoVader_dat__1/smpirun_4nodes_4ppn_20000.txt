================================================================================
HPLinpack 2.2  --  High-Performance Linpack benchmark  --   February 24, 2016
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   20000 
NB     :     120 
PMAP   : Row-major process mapping
P      :       4 
Q      :       4 
PFACT  :    Left    Crout    Right 
NBMIN  :       2        4 
NDIV   :       2 
RFACT  :    Left    Crout    Right 
BCAST  :   1ring 
DEPTH  :       0 
SWAP   : Mix (threshold = 64)
L1     : transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00L2L2       20000   120     4     4               9.60              5.557e+02
HPL_pdgesv() start time Thu Jun  9 13:17:36 2022

HPL_pdgesv() end time   Thu Jun  9 13:19:47 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0034198 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00L2L4       20000   120     4     4               9.58              5.566e+02
HPL_pdgesv() start time Thu Jun  9 13:19:58 2022

HPL_pdgesv() end time   Thu Jun  9 13:22:09 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0034866 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00L2C2       20000   120     4     4               9.61              5.552e+02
HPL_pdgesv() start time Thu Jun  9 13:22:20 2022

HPL_pdgesv() end time   Thu Jun  9 13:24:31 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0034198 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00L2C4       20000   120     4     4               9.58              5.570e+02
HPL_pdgesv() start time Thu Jun  9 13:24:42 2022

HPL_pdgesv() end time   Thu Jun  9 13:26:53 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0033035 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00L2R2       20000   120     4     4               9.58              5.569e+02
HPL_pdgesv() start time Thu Jun  9 13:27:04 2022

HPL_pdgesv() end time   Thu Jun  9 13:29:15 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0034198 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00L2R4       20000   120     4     4               9.58              5.567e+02
HPL_pdgesv() start time Thu Jun  9 13:29:26 2022

HPL_pdgesv() end time   Thu Jun  9 13:31:37 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0030047 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00C2L2       20000   120     4     4               9.58              5.570e+02
HPL_pdgesv() start time Thu Jun  9 13:31:48 2022

HPL_pdgesv() end time   Thu Jun  9 13:33:59 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0034198 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00C2L4       20000   120     4     4               9.59              5.562e+02
HPL_pdgesv() start time Thu Jun  9 13:34:10 2022

HPL_pdgesv() end time   Thu Jun  9 13:36:21 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0034866 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00C2C2       20000   120     4     4               9.59              5.563e+02
HPL_pdgesv() start time Thu Jun  9 13:36:33 2022

HPL_pdgesv() end time   Thu Jun  9 13:38:43 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0034198 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00C2C4       20000   120     4     4               9.56              5.580e+02
HPL_pdgesv() start time Thu Jun  9 13:38:55 2022

HPL_pdgesv() end time   Thu Jun  9 13:41:06 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0033035 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00C2R2       20000   120     4     4               9.58              5.569e+02
HPL_pdgesv() start time Thu Jun  9 13:41:17 2022

HPL_pdgesv() end time   Thu Jun  9 13:43:28 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0034198 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00C2R4       20000   120     4     4               9.58              5.566e+02
HPL_pdgesv() start time Thu Jun  9 13:43:39 2022

HPL_pdgesv() end time   Thu Jun  9 13:45:50 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0030047 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00R2L2       20000   120     4     4               9.60              5.559e+02
HPL_pdgesv() start time Thu Jun  9 13:46:01 2022

HPL_pdgesv() end time   Thu Jun  9 13:48:12 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0034198 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00R2L4       20000   120     4     4               9.58              5.570e+02
HPL_pdgesv() start time Thu Jun  9 13:48:23 2022

HPL_pdgesv() end time   Thu Jun  9 13:50:34 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0034866 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00R2C2       20000   120     4     4               9.58              5.565e+02
HPL_pdgesv() start time Thu Jun  9 13:50:45 2022

HPL_pdgesv() end time   Thu Jun  9 13:52:56 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0034198 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00R2C4       20000   120     4     4               9.57              5.572e+02
HPL_pdgesv() start time Thu Jun  9 13:53:07 2022

HPL_pdgesv() end time   Thu Jun  9 13:55:18 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0033035 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00R2R2       20000   120     4     4               9.57              5.576e+02
HPL_pdgesv() start time Thu Jun  9 13:55:29 2022

HPL_pdgesv() end time   Thu Jun  9 13:57:40 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0034198 ...... PASSED
================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR00R2R4       20000   120     4     4               9.58              5.566e+02
HPL_pdgesv() start time Thu Jun  9 13:57:51 2022

HPL_pdgesv() end time   Thu Jun  9 14:00:02 2022

--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0030047 ...... PASSED
================================================================================

Finished     18 tests with the following results:
             18 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
