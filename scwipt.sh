#!/bin/bash

for rep in $(seq 1 100); do
    size=10000
    ppn=4
    nodes=4

    S4BXI_SMPI_IMPLEM=0 ./s4bxi_run_vanilla.sh \
        -i src/HPL/HPL.dat.dist \
        -p src/HPL/patch_hpl_vanilla_S4BXI_modelChange_manyLogs.patch \
        -f ManyLogs_$rep \
        -o modelChange \
        -m src/HPL/Make.S4BXI \
        -s $size -c $ppn -N $nodes
    S4BXI_SMPI_IMPLEM=0 ./s4bxi_run_vanilla.sh \
        -i src/HPL/HPL.dat.dist \
        -p src/HPL/patch_hpl_vanilla_S4BXI_fullS4BXI_manyLogs.patch \
        -f ManyLogs_$rep \
        -o s4bxi \
        -m src/HPL/Make.S4BXI \
        -s $size -c $ppn -N $nodes
    S4BXI_SMPI_IMPLEM=1 ./s4bxi_run_vanilla.sh \
        -i src/HPL/HPL.dat.dist \
        -p src/HPL/patch_hpl_vanilla_S4BXI_fullSMPI_manyLogs.patch \
        -f ManyLogs_$rep \
        -o smpi \
        -m src/HPL/Make.S4BXI \
        -s $size -c $ppn -N $nodes

    for i in $(seq 0 15) ; do 
        Rscript plotLogs.R \
            ./logs/ManyLogs_$rep/s4bxi_$size/$i.csv \
            ./logs/ManyLogs_$rep/smpi_$size/$i.csv \
            ./logs/ManyLogs_$rep/modelChange_$size/$i.csv \
            ./logs/ManyLogs_$rep/$i.png
    done
done