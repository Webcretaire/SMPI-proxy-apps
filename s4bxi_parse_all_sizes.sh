#!/bin/bash

folder=$1 # Folder of the data files
prefix=$2 # Prefix of the experiment (including last "_", not including size)
ranks=${3:-16} # Number of ranks in the job

for size in 1250 1875 2500 3750 5000 7500 10000 15000 20000 ; do 
    ./s4bxi_parse_logs.sh $folder "${prefix}${size}" $ranks
done
